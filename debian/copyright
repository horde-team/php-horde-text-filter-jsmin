Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Horde Text Filter - Jsmin PHP Driver
Upstream-Contact: Horde <horde@lists.horde.org>
                  Horde development <dev@lists.horde.org>
Source: http://pear.horde.org/
Disclaimer: This package is considered non-free because it is adds a "shall be
 used for Good, not Evil" requirement.

Files: *
Copyright: 2009-2016, Horde LLC (http://www.horde.org)
           2002, Douglas Crockford (www.crockford.com)
           2008, Ryan Grove <ryan@wonko.com>
License: Douglas-Crockford-Good-not-Evil-License

Files: debian/*
Copyright: 2013-2019, Mathieu Parent <math.parent@gmail.com>
           2020, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-2.1+ or Douglas-Crockford-Good-not-Evil-License

License: Douglas-Crockford-Good-not-Evil-License
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 The Software shall be used for Good, not Evil.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
